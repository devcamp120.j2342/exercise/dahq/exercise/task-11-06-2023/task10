package com.devcamp.schoolapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.schoolapi.model.Classroom;
import com.devcamp.schoolapi.service.ClassroomService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ClassroomController {
    @Autowired
    private ClassroomService classroomService;

    @GetMapping("/classrooms")
    public ArrayList<Classroom> classroomsList() {
        ArrayList<Classroom> classrooms = classroomService.allClassrooms();
        return classrooms;
    }

    @GetMapping("/classroom-noNumber/{noNumber}")
    public ArrayList<Classroom> classroomsNoNumber(@PathVariable int noNumber) {
        ArrayList<Classroom> classrooms = classroomService.classroomsByNoNumber(noNumber);
        return classrooms;
    }

}
