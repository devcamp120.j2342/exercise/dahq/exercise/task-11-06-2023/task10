package com.devcamp.schoolapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.schoolapi.model.Classroom;

@Service
public class ClassroomService {
    Classroom classroom1 = new Classroom(1, "ClassA1", 30);
    Classroom classroom2 = new Classroom(2, "ClassA2", 20);
    Classroom classroom3 = new Classroom(3, "ClassA3", 10);
    Classroom classroom4 = new Classroom(1, "ClassB1", 50);
    Classroom classroom5 = new Classroom(2, "ClassB2", 70);
    Classroom classroom6 = new Classroom(3, "ClassB3", 5);
    Classroom classroom7 = new Classroom(1, "ClassC1", 3);
    Classroom classroom8 = new Classroom(2, "ClassC2", 30);
    Classroom classroom9 = new Classroom(3, "ClassC3", 90);

    public ArrayList<Classroom> classroomsA() {
        ArrayList<Classroom> classrooms = new ArrayList<>();
        classrooms.add(classroom1);
        classrooms.add(classroom2);
        classrooms.add(classroom3);
        return classrooms;

    }

    public ArrayList<Classroom> classroomsB() {
        ArrayList<Classroom> classrooms = new ArrayList<>();
        classrooms.add(classroom4);
        classrooms.add(classroom6);
        classrooms.add(classroom5);
        return classrooms;

    }

    public ArrayList<Classroom> classroomsC() {
        ArrayList<Classroom> classrooms = new ArrayList<>();
        classrooms.add(classroom9);
        classrooms.add(classroom7);
        classrooms.add(classroom8);
        return classrooms;

    }

    public ArrayList<Classroom> allClassrooms() {
        ArrayList<Classroom> classrooms = new ArrayList<>();
        classrooms.addAll(classroomsA());
        classrooms.addAll(classroomsB());
        classrooms.addAll(classroomsC());
        return classrooms;

    }

    public ArrayList<Classroom> classroomsByNoNumber(int noNumber) {
        ArrayList<Classroom> classrooms = allClassrooms();
        ArrayList<Classroom> classroomsRe = new ArrayList<>();
        for (Classroom classroom : classrooms) {
            if (classroom.getNoStudent() > noNumber) {
                classroomsRe.add(classroom);
            }
        }
        return classroomsRe;
    }

}
